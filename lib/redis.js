var config = require('config');
var redis = require('redis');

var opts = {
  host: config.redis.host,
  port: config.redis.port
};

if (config.redis.password) opts.password = config.redis.password;

var client = redis.createClient(opts);
client.select(config.redis.dbnumber);

module.exports = client;
