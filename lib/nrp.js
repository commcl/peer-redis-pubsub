const config = require('config');
const redis = require('redis');
const NRP = require('node-redis-pubsub');

var opts = {
  host: config.redis.host,
  port: config.redis.port
};

if (config.redis.password) opts.password = config.redis.password;

var redisPub = redis.createClient(opts);
var redisSub = redis.createClient(opts);

redisPub.select(config.redis.dbnumber);
redisSub.select(config.redis.dbnumber);

var nrp_config = {
  emitter: redisPub,
  receiver: redisSub,
};

var NodeRedisPubSub = function() {
	return new NRP(nrp_config);
};

module.exports = NodeRedisPubSub;
